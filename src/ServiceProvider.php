<?php
namespace Kemweb\LaravelDefault;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;
use Illuminate\Support\Facades\Schema;

class ServiceProvider extends IlluminateServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        if (env('DB_OLD_MYSQL'))
        {
            // https://laravel-news.com/laravel-5-4-key-too-long-error
            Schema::defaultStringLength(191);
        }

        // Bugsnag ohne IP des Benutzers
        // Wenn Bugsnag geladen und des API Key gesetzt ist
        if ($this->app->has('bugsnag') && env('BUGSNAG_API_KEY'))
        {
            Bugsnag::registerCallback(function ($report) {
                // Benutzer IP überschreiben
                $report->setUser([
                    'id' => null,
                ]);
                $report->setMetaData([
                    'request' => [
                        'clientIp' => null
                    ]
                ]);
            });
        }

        if ($this->app->runningInConsole() && function_exists('posix_geteuid'))
        {
            if (posix_geteuid() === 0)
            {
                echo "\033[31mDo you really want to run artisan as root / super user? [y/N]: \033[0m";
                $line = readline();
                if (strtolower(trim($line)) !== 'y')
                {
                    die();
                }
            }

        }
    }

}
