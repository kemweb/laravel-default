# KEMWEB Laravel Default Paket

Dieses Paket sollte alles enthalten was wir bei neuen Laravel Projekten immer machen.

Siehe auch [Laravel Tipps](https://kemweb.atlassian.net/wiki/spaces/KWA/pages/16586160/Laravel+Tipps).

## Installation

    composer config repositories.kemweb-laravel-default vcs https://bitbucket.org/kemweb/laravel-default.git
    composer require kemweb/laravel-default

## Setup / Config
### Bugsnag
`.env` Eintrag bei alter MySQL (< v5.7.7) MySQL einfügen (Fix: Specified key was too long; max key length is 767 bytes).

    DB_OLD_MYSQL=true

In `.env` Bugsnag Key einfügen:

    BUGSNAG_API_KEY=your-api-key-here

In `config/app.php` Bugsnag Service Provider einfügen:

    Bugsnag\BugsnagLaravel\BugsnagServiceProvider::class
    
## Inhalt / Features

Pakete:

    * Bugsnag (ohne IPs): https://docs.bugsnag.com/platforms/php/laravel/
    * Composer Sicherheitscheck: roave/security-advisories
